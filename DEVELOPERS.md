# README for developers

## Getting started

```bash
# Install dependencies
pipenv install --dev
# Enter the virtualenv
pipenv shell
# Generate GraphQL bindings
ariadne-codegen
```

## Code structure

 - `brezavta_api/` - source root
   - `backends/` - code that connects to backing services
   - `endpoints/` - FastApi endpoints (one router per file)
   - `models/` - Pydantic models for the API
   - `main.py` - this is where FastAPI is initialized

## GraphQL

GraphQL queries should be defined in `.gql` files in the `backends/` folder, preferable named similarly to the Python files they are used in. When a query is changed, run `ariadne-codegen` to regenerate the bindings. If you're using PyCharm, this should happen automatically (you might need to go under File Watchers and enable it).

Let's say you define `micromobility.gql` (query names MUST be in snake_case!):
```graphql
query get_micromobility_stations {
    vehicleRentalStations {
        stationId
        name
        lat
        lon
    }
}
```

You can call it from Python with the following code:
```python
from generated.graphql_client import Client
client = Client(url=GRAPHQL_URL)
# or if you're using OpenTripPlanner
from .opentripplanner import client

stations = await client.get_micromobility_stations()

for station in stations.vehicleRentalStations:
    print(station.name)
```