from typing import List
from fastapi import APIRouter, Query

from fastapi_cache import FastAPICache
from fastapi_cache.decorator import cache
from fastapi_cache.backends.inmemory import InMemoryBackend

from brezavta_api.models.stops import Stop, Arrival, StopDetails, Schedule

from ..backends import stops

from datetime import datetime

router = APIRouter(
    prefix="/stops",
    tags=["Stops"],
)

FastAPICache.init(backend=InMemoryBackend())

@router.get(
    "/",
    response_model=List[Stop],
)
@cache(expire=86400)
async def list_stops():
    
    stops_results = await stops.get_stops()

    return stops_results

@router.get(
    "/{stop_id}",
    response_model=StopDetails,
    description="Get stop details"
)
@cache(expire=15)
async def get_stop(stop_id: str, date: str = Query(None), current: str = Query(None)):
    
        if date is None:
            date = datetime.now().strftime('%Y%m%d')
        else:
            date = datetime.strptime(date, '%Y%m%d').strftime('%Y%m%d')
    
        stop_results = await stops.get_details(stop_id, date, True if current == 'true' else False)
    
        return stop_results

@router.get(
    "/{stop_id}/arrivals",
    response_model=List[Arrival],
    description="List arrivals for a stop, optionally filtered by date (YYYYMMDD) and current time (true/false)"
)
@cache(expire=15)
async def list_arrivals(stop_id: str, date: str = Query(None), current: str = Query(None)):
    
    if date is None:
        date = datetime.now().strftime('%Y%m%d')
    else:
        date = datetime.strptime(date, '%Y%m%d').strftime('%Y%m%d')

    arrivals_results = await stops.get_arrivals(stop_id, date, True if current == 'true' else False)

    return arrivals_results

@router.get(
    "/{stop_id}/schedule/{date}",
    response_model=Schedule,
    description="List schedule for a stop, filtered by date (YYYYMMDD)"
)
@cache(expire=60)
async def list_schedule(stop_id: str, date: str):
    
    date = datetime.strptime(date, '%Y%m%d').strftime('%Y%m%d')

    schedule_result = await stops.get_schedule(stop_id, date)

    return schedule_result
     