from typing import List
from fastapi import APIRouter, Query
from pydantic_geojson import LineStringModel
from brezavta_api.models.plan import PlanWrapper, PlanTimeType

from ..backends import plan

from datetime import datetime

router = APIRouter(
    prefix="/plan",
    tags=["plan"],
)

@router.get(
    "/{transport_modes}/{from_lon_lat}/{to_lon_lat}",
    description="Gets plan for trip. Transport modes are comma separated, e.g. WALK,BUS. From and to lon,lat are comma separated, e.g. 14.5058,46.0569.",
    response_model=PlanWrapper,
)
async def get_plan(transport_modes: str, 
                   from_lon_lat: str, 
                   to_lon_lat: str, 
                   date: str = Query(datetime.now().strftime('%Y-%m-%d')), 
                   time: str = Query(datetime.now().strftime('%H:%M')), 
                   type: PlanTimeType = Query(PlanTimeType.DEPART),
                   wheelchair: bool = Query(False),
                   page_cursor: str = Query(None),
                   num_itinearies: int = Query(5, ge=1, le=10)):

    from_location = from_lon_lat.split(',')
    to_location = to_lon_lat.split(',')
    
    plan_results = await plan.get_plan(
        from_point={
            'lat': from_location[1],
            'lon': from_location[0],
        },
        to_point={
            'lat': to_location[1],
            'lon': to_location[0],
        },
        date=date,
        time=time,
        transport_modes=transport_modes,
        page_cursor=page_cursor,
        arrive_by=True if type == PlanTimeType.ARRIVE else False,
        wheelchair=wheelchair,
        num_itinearies=num_itinearies
    )


    return plan_results
