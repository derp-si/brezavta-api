from typing import List
from fastapi import APIRouter, Query

from fastapi_cache import FastAPICache
from fastapi_cache.decorator import cache
from fastapi_cache.backends.inmemory import InMemoryBackend

from brezavta_api.models.vehicles import VehicleLocation

from ..backends import trips

from datetime import datetime

router = APIRouter(
    prefix="/vehicles",
    tags=["Vehicles"],
)

FastAPICache.init(backend=InMemoryBackend())

@router.get(
    "/locations",
    response_model=List[VehicleLocation],
)

@cache(expire=5)
async def list_vehicles():
    
    vehicles_results = await trips.get_all_vehicle_locations()

    return vehicles_results