from typing import List
from fastapi import APIRouter, Query

from fastapi_cache import FastAPICache
from fastapi_cache.decorator import cache
from fastapi_cache.backends.inmemory import InMemoryBackend

from brezavta_api.models.trips import Trip, LineStringTrip, TripWithGeometry
from brezavta_api.models.vehicles import VehicleLocation, Vehicle

from ..backends import trips

from datetime import datetime

router = APIRouter(
    prefix="/trips",
    tags=["Trips"],
)

FastAPICache.init(backend=InMemoryBackend())

@router.get(
    "/{trip_id}",
    response_model=Trip,
)
async def get_trip(trip_id: str, date: str = Query(datetime.now().strftime('%Y%m%d'))):
    
    trip_results = await trips.get_trip(trip_id, date)

    if trip_results is None:
        return None

    return trip_results

@router.get(
    "/{trip_id}/geometry",
    response_model=LineStringTrip,
)
@cache(expire=1200)
async def get_trip_geometry(trip_id: str):
    
    trip_geometry_results = await trips.get_trip_geometry(trip_id)

    return trip_geometry_results

@router.get(
    "/{trip_id}/ui",
    response_model=TripWithGeometry,
    description="Returns trip with geometry."
)
async def get_trip_with_geometry(trip_id: str, date: str = Query(datetime.now().strftime('%Y%m%d'))):
    
    trip_results = await trips.get_trip(trip_id, date)

    if trip_results is None:
        return None

    trip_geometry_results = await trips.get_trip_geometry_from_cache(trip_id)

    return TripWithGeometry(trip=trip_results, geometry=trip_geometry_results)

@router.get(
    "/{trip_id}/vehicles",
    response_model=List[VehicleLocation],
    description="List vehicles for one or more trips. Multiple trip IDs can be provided as a comma-separated list."
)
async def get_vehicle_locations(trip_id: str):
    
    vehicles_results = await trips.get_vehicle_locations(trip_id)

    return vehicles_results