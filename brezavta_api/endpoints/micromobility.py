from typing import List
from fastapi import APIRouter, Query

from fastapi_cache import FastAPICache
from fastapi_cache.decorator import cache
from fastapi_cache.backends.inmemory import InMemoryBackend

from brezavta_api.models.micromobility import Micromobility, MicromobilityDetail, Vehicle

from ..backends import micromobility


router = APIRouter(
    prefix="/micromobility",
    tags=["Micromobility"],
)

FastAPICache.init(backend=InMemoryBackend())

@router.get(
    "/",
    response_model=List[Micromobility],
)
@cache(expire=60)
async def get_micromobility():
    micromobility_results = await micromobility.get_micromobility()
    return micromobility_results


@router.get('/{micromobility_id}', response_model=MicromobilityDetail)
@cache(expire=60)
async def get_micromobility_detail(micromobility_id: str):
    station_detail = await micromobility.get_micromobility_detail(micromobility_id)
    return station_detail