from typing import List
from fastapi import APIRouter

from fastapi_cache import FastAPICache
from fastapi_cache.decorator import cache
from fastapi_cache.backends.inmemory import InMemoryBackend
from brezavta_api.models.geocoding import SearchResult

from ..backends import nominatim
from ..backends import pelias
from ..backends import opentripplanner

router = APIRouter(
    prefix="/geocoding",
    tags=["Geocoding"],
)

FastAPICache.init(backend=InMemoryBackend())

@router.get(
    "/search",
    response_model=List[SearchResult],
)
@cache(expire=43200)
async def search(string, focus_lat: float = None, focus_lon: float = None):
    data = await pelias.pelias_autocomplete(string, focus_lat, focus_lon)
    results = pelias.convert_places_autocomplete(data, string)

    return results

@router.get(
    "/reverse_geocode",
)
async def reverse_geocode(lat: float, lon: float):
    data = await pelias.reverse_geocode(lat, lon)
    results = pelias.convert_reverse_geocode(data, lat, lon)
    return results
