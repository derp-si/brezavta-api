from typing import List
from fastapi import APIRouter, Query

from fastapi_cache import FastAPICache
from fastapi_cache.decorator import cache
from fastapi_cache.backends.inmemory import InMemoryBackend

from brezavta_api.models.parking import Parking

from ..backends import parking

from datetime import datetime

router = APIRouter(
    prefix="/parking",
    tags=["Parking"],
)

FastAPICache.init(backend=InMemoryBackend())

@router.get(
    "/",
    response_model=List[Parking],
)
@cache(expire=60)
async def get_parking():
    
    parking_results = await parking.get_parking()

    return parking_results