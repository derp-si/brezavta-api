
from enum import Enum
from typing import Optional
from pydantic import BaseModel

class ParkingEnum(str, Enum):
    NO_DATA = 'NO_DATA'
    MANY = 'MANY'
    SOME = 'SOME'
    NONE = 'NONE'

class Parking(BaseModel):
    id: str
    name: str
    realtime: bool
    lat: float
    lon: float
    capacity: int
    available: Optional[int]
    icon: ParkingEnum
    