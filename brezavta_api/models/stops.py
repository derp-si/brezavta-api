
from enum import Enum
from typing import Optional
from pydantic import BaseModel
from brezavta_api.models.alerts import Alert

class StopType(str, Enum):
    BUS = 'BUS'
    TRAM = 'TRAM'
    SUBWAY = 'SUBWAY'
    RAIL = 'RAIL'
    FERRY = 'FERRY'
    CABLE_CAR = 'CABLE_CAR'
    GONDOLA = 'GONDOLA'
    FUNICULAR = 'FUNICULAR'

class Stop(BaseModel):
    id: str
    gtfs_id: str
    code: Optional[str]
    type: StopType
    background_color: str = "#000000"
    icon_color: str = "#FFFFFF"
    name: str
    lat: float
    lon: float
    
class Arrival(BaseModel):
    agency_id: str
    agency_name: str
    route_id: str
    route_short_name: str
    route_color_background: Optional[str]
    route_color_text: Optional[str]
    trip_id: str
    trip_headsign: Optional[str]
    realtime: bool
    realtime_status: str
    passed: bool
    arrival_scheduled: int
    arrival_realtime: Optional[int]
    arrival_delay: Optional[int]
    departure_scheduled: int
    departure_realtime: Optional[int]
    departure_delay: Optional[int]
    alerts: Optional[list[Alert]]

class ScheduleEntry(BaseModel):
    trip_id: str
    departure_time: int
    realtime: bool

class ScheduleRouteEntry(BaseModel):
    route_id: str
    route_short_name: str
    route_color_background: Optional[str]
    route_color_text: Optional[str]
    trip_headsign: Optional[str]
    times: list[ScheduleEntry]

class Schedule(Stop):
    schedule: list[ScheduleRouteEntry]

class StopDetails(Stop):
    arrivals: list[Arrival]
    alerts: list[Alert]