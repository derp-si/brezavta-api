
from enum import Enum
from typing import Optional
from pydantic import BaseModel

class MicromobilityForm(str, Enum):
    BICYCLE = 'BICYCLE'
    CAR = 'CAR'
    SCOOTER = 'SCOOTER'

class MicromobilityType(str, Enum):
    STATION = 'STATION'
    FLOATING = 'FLOATING'
    
class MicromobilityAvailability(str, Enum):
    NO_DATA = 'NO_DATA'
    MANY = 'MANY'
    SOME = 'SOME'
    NONE = 'NONE'
    SINGLE = 'SINGLE'

class Micromobility(BaseModel):
    id: str
    type: MicromobilityType
    form: MicromobilityForm
    name: str
    lat: float
    lon: float
    vehicles: int
    spaces: int
    network: Optional[str]
    active: bool
    icon: MicromobilityAvailability

class MicromobilityDetail(Micromobility):
    vehicle_list: list['Vehicle']
    image_url: Optional[str] = None

class Vehicle(BaseModel):
    id: str
    type: MicromobilityForm
    model: Optional[str] = None
    image_url: Optional[str] = None
    deeplink: Optional[str] = None
    plate: Optional[str] = None
    battery: Optional[float] = None
    range: Optional[float] = None
    price_start: Optional[float] = None
    price_minimum: Optional[float] = None
    price_per_min: Optional[float] = None
    price_per_km: Optional[float] = None
