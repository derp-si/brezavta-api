
from enum import Enum
from typing import Optional
from pydantic import BaseModel
from pydantic_geojson import LineStringModel
from brezavta_api.models.alerts import Alert
from brezavta_api.models.stops import Stop

class PlanTimeType(str, Enum):
    DEPART = 'DEPART'
    ARRIVE = 'ARRIVE'

class LegType(str, Enum):
    WALK = 'WALK'
    BUS = 'BUS'
    TRAM = 'TRAM'
    SUBWAY = 'SUBWAY'
    RAIL = 'RAIL'
    FERRY = 'FERRY'
    CABLE_CAR = 'CABLE_CAR'
    GONDOLA = 'GONDOLA'
    FUNICULAR = 'FUNICULAR'
    BICYCLE = 'BICYCLE'
    SCOOTER = 'SCOOTER'
    CARPOOL = 'CARPOOL'
    TAXI = 'TAXI'
    TRANSIT = 'TRANSIT'
    CAR = 'CAR'
    UNKNOWN = 'UNKNOWN'

class LegPoint(str, Enum):
    ORIGIN = 'ORIGIN'
    DESTINATION = 'DESTINATION'
    INTERMEDIATE = 'INTERMEDIATE'

class Point(BaseModel):
    lat: float
    lon: float
    departure_time: int
    arrival_time: int
    name: str
    type: LegPoint = LegPoint.INTERMEDIATE
    vertex_type: str
    gtfs_id: Optional[str]
    micromobility_id: Optional[str]
    # TODO: We can also implement vehicleRentalStation, Stop, rentalVehicle and vehicleParking here

class Direction(str, Enum):
    DEPART = 'DEPART'
    ARRIVE = 'ARRIVE'
    CONTINUE = 'CONTINUE'
    LEFT = 'LEFT'
    RIGHT = 'RIGHT'
    SLIGHTLY_LEFT = 'SLIGHTLY_LEFT'
    SLIGHTLY_RIGHT = 'SLIGHTLY_RIGHT'
    HARD_LEFT = 'HARD_LEFT'
    HARD_RIGHT = 'HARD_RIGHT'
    UTURN_LEFT = 'UTURN_LEFT'
    UTURN_RIGHT = 'UTURN_RIGHT'
    ELEVATOR = 'ELEVATOR'
    ESCALATOR = 'ESCALATOR'
    STAIRS = 'STAIRS'
    RAMP = 'RAMP'
    WALKWAY = 'WALKWAY'
    BOARD = 'BOARD'
    ALIGHT = 'ALIGHT'
    UNKNOWN = 'UNKNOWN'

class Step(BaseModel):
    distance: float
    lat: float
    lon: float
    relative_direction: Direction
    street_name: str
    stay_on: bool

class Trip(BaseModel):
    id: str
    short_name: str
    long_name: Optional[str]
    headsign: str
    route_id: str
    gtfs_id: str
    color: str="#000000"
    text_color: str="#FFFFFF"

class StopWithTime(Stop):
    arrival_time: Optional[int]
    departure_time: Optional[int]

class Leg(BaseModel):
    mode: LegType
    is_rental: bool
    start_time: int
    end_time: int
    from_point: Point
    to_point: Point
    geometry: Optional[LineStringModel]
    trip: Optional[Trip]
    alerts: Optional[list[Alert]]
    realtime: Optional[bool]
    realtime_status: Optional[str]
    intermediate_stops: Optional[list[StopWithTime]]
    steps: Optional[list[Step]]

class Plan(BaseModel):
    start_time: int
    end_time: int
    legs: list[Leg]

class PlanWrapper(BaseModel):
    previous_page: Optional[str]
    next_page: Optional[str]
    plans: list[Plan]