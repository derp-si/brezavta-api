
from enum import Enum
from typing import Optional
from pydantic import BaseModel

class Alert(BaseModel):
    id: str
    language: str
    header: str
    description: str
    effect: str
    severity_level: str
    cause: str
    trip_id: Optional[str]
    stop_id: Optional[str]
    url: Optional[str]
    start_time: Optional[int]
    end_time: Optional[int]