
from enum import Enum
from typing import Optional
from pydantic import BaseModel


class ResultType(str, Enum):
    TRANSIT_STOP = "transit_stop"
    RENTAL_STATION = "rental_station"
    GEOCODE = "geocode"


class SearchResult(BaseModel):
    label: str
    lat: float
    lon: float
    confidence: float

    description: Optional[str] = None

    otp_code: Optional[str] = None
    type: ResultType

