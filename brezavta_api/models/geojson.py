
from enum import Enum
from typing import Optional
from pydantic import BaseModel

class LineString(BaseModel):
    type: str
    coordinates: list[list[float]]