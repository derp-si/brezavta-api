
from enum import Enum
from typing import Optional
from pydantic import BaseModel
from brezavta_api.models.stops import Stop

class Vehicle(BaseModel):
    id: str
    gtfs_id: str
    model: Optional[str]
    low_floor: Optional[bool]
    plate: Optional[str]
    operator_id: str
    operator_name: str
    image_url: Optional[list[str]]

class VehicleLocation(BaseModel):
    vehicle: Vehicle
    stop: Optional[Stop]
    stop_status: Optional[str]
    lat: float
    lon: float
    heading: Optional[float]
    speed: Optional[float]
    color: Optional[str]
    trip_id: str
    trip_headsign: str
    route_short_name: str
    timestamp: int