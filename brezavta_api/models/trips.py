
from enum import Enum
from typing import Optional
from pydantic import BaseModel
from brezavta_api.models.stops import Stop
from brezavta_api.models.alerts import Alert
from pydantic_geojson import LineStringModel

class TripType(str, Enum):
    BUS = 'BUS'
    TRAM = 'TRAM'
    SUBWAY = 'SUBWAY'
    RAIL = 'RAIL'
    FERRY = 'FERRY'
    CABLE_CAR = 'CABLE_CAR'
    GONDOLA = 'GONDOLA'
    FUNICULAR = 'FUNICULAR'

class StopTime(BaseModel):
    stop: Stop
    sequence: int
    realtime: bool
    realtime_status: str
    arrival_scheduled: int
    arrival_realtime: Optional[int]
    arrival_delay: Optional[int]
    departure_scheduled: int
    departure_realtime: Optional[int]
    departure_delay: Optional[int]
    alerts: list[Alert]
    passed: bool


class Trip(BaseModel):
    id: str
    gtfs_id: str
    route_id: str
    route_short_name: str
    trip_type: TripType
    trip_headsign: Optional[str]
    stop_times: list[StopTime]
    color: str="#000000"
    text_color: str="#FFFFFF"

class LineStringProperties(BaseModel):
    color: str

class LineStringTrip(LineStringModel):
    properties: LineStringProperties
    
class TripWithGeometry(BaseModel):
    trip: Trip
    geometry: LineStringTrip