from typing import Tuple, Optional, TypeVar

from fastapi_cache.backends import Backend


class DummyCacheBackend(Backend):

    async def get_with_ttl(self, key: str) -> Tuple[int, Optional[str]]:
        return 0, None

    async def get(self, key: str) -> str:
        pass

    async def set(self, key: str, value: str, expire: int = None):
        pass

    async def clear(self, namespace: str = None, key: str = None) -> int:
        return 0


T = TypeVar('T', bound=dict)
def find_by(lst: list[T], key: str, value: str) -> T | None:
  for item in lst:
    if item[key] == value:
      return item

def many_some_none(capacity, value):
    return 'MANY' if value > 0.3 * capacity else 'SOME' if value > 0 else 'NONE'
