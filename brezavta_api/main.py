import asyncio
import importlib
import logging
import time
from collections import defaultdict
from datetime import datetime, timedelta
from types import TracebackType

import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from starlette.requests import Request
from starlette.types import ASGIApp, Receive, Send, Scope
from starlette.responses import Response

from apscheduler.schedulers.asyncio import AsyncIOScheduler
from apscheduler.triggers.cron import CronTrigger


from brezavta_api import config

from brezavta_api.backends.trips import get_vehicle_positions, get_vehicle_positions_metadata, load_trip_updates_for_trips
from brezavta_api.backends.stops import load_trip_updates



scheduler = AsyncIOScheduler()


###############
#   LOGGING   #
###############

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("rest_api")


##############
#   SENTRY   #
##############

if config.USE_SENTRY:
    # If an error happens on the same line, ignore it for this amount of time (so we don't burn through our quota)
    SENTRY_MAX_PER_LINE = timedelta(hours=10).total_seconds()
    last_sent = defaultdict(float)

    def sentry_filter(event, hint):
        if 'exc_info' in hint:
            exc_type, exc_value, tb = hint['exc_info']
            tb: TracebackType
            fingerprint = f'{tb.tb_frame.f_lineno}:{tb.tb_frame.f_code.co_filename}'
            if time.time() - last_sent[fingerprint] > SENTRY_MAX_PER_LINE:
                last_sent[fingerprint] = time.time()
                return event
            return None
        return event

    logger.info("Connecting to Sentry")
    import sentry_sdk  # noqa
    sentry_sdk.init(
        dsn=config.SENTRY_DSN,
        before_send=sentry_filter,
        traces_sample_rate=0,
    )


######################
#   FASTAPI SERVER   #
######################

# Set to True when all modules have loaded (used for readiness probe)
READY = False

# The actual server
app = FastAPI(
    title="Brezavta.si backend",
)

#########################
#   LOAD SITE MODULES   #
#########################

ENDPOINTS = []
for name in ('parking', 'micromobility', 'trips', 'vehicles', 'stops', 'geocoding', 'plan'):
    try:
        endpoint_group = importlib.import_module('brezavta_api.endpoints.' + name)
        ENDPOINTS.append(endpoint_group)
    except:  # noqa
        import traceback
        traceback.print_exc()

for endpoint_group in ENDPOINTS:
    try:
        app.include_router(endpoint_group.router)
    except:  # noqa
        logger.exception("Error while including router %s" % endpoint_group.__name__)


############################
#   SITE STARTUP METHODS   #
############################

@app.on_event("startup")
async def on_startup():
    # Start scheduler

    # Some sites have async startup methods, so we can only call them in an async context
    for site in ENDPOINTS:
        try:
            if hasattr(site, 'setup') and callable(site.setup):
                await site.setup()
        except:  #noqa
            logger.exception("Error while setting up site %s" % site.__name__)

    scheduler.add_job(get_vehicle_positions_metadata, trigger='interval', seconds=600, id="load_metadata_locations", next_run_time=datetime.now())
    scheduler.add_job(get_vehicle_positions, trigger='interval', seconds=10, id="load_locations")
    scheduler.add_job(load_trip_updates, trigger='interval', seconds=10, id="load_trip_updates")
    scheduler.add_job(load_trip_updates_for_trips, trigger='interval', seconds=10, id="load_trip_updates_2")
    scheduler.start()

    global READY
    READY = True



########################
#   GLOBAL ENDPOINTS   #
########################

@app.get('/robots.txt')
async def robots():
    return Response(content='User-agent: *\nDisallow: /', media_type='text/plain')

@app.get('/', include_in_schema=False)
async def home():
    return 'OK'

@app.get('/ready', description="Returns 200 OK only when all modules have loaded", include_in_schema=False)
async def ready():
    if READY:
        return Response(status_code=200, content='OK')
    else:
        return Response(status_code=503, content='NOT READY')

@app.get('/debug', include_in_schema=False)
async def debug(request: Request):
    return {
        'headers': dict(request.headers),
        'ip': request.client.host,
        'ok': True
    }


if __name__ == "__main__":
    # Run with uvicorn
    uvicorn.run(app, host="0.0.0.0", port=42069)
