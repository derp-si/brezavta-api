from aiographql.client import GraphQLClient
import aiohttp

from brezavta_api import config
from brezavta_api.models.stops import Stop, Arrival, Alert, StopDetails, ScheduleEntry, ScheduleRouteEntry, Schedule

from datetime import datetime, timedelta
import pytz
import math
import re

from generated.graphql_client.client import gql

client = GraphQLClient(
    endpoint=config.OTP_URL + '/otp/gtfs/v1',
)

RT_TU_URL = 'https://rt.gtfs.ijpp.prometko.si/combined/rt/tu?type=json'

Q_SEARCH_STOPS = gql("""
query Stops {
  stops {
    id
    gtfsId
    vehicleMode
    code
    name
    lat
    lon
 }
}
""")

Q_SEARCH_ALERTS = """
query Alerts {
  alerts {
    id
    feed
    alertHeaderTextTranslations {
      text
      language
    }
    alertDescriptionTextTranslations {
      text
      language
    }
    alertEffect
    alertUrlTranslations {
      text
      language
    }
    alertSeverityLevel
    alertCause
    effectiveStartDate
    effectiveEndDate
    entities {
      __typename
      ... on StopOnTrip {
        trip {
          gtfsId
        }
        stop {
          gtfsId
        }
      }
    }
  }
}
"""

Q_SEARCH_ARRIVALS_BY_STOP = """
query GetStopTimes($id: String!, $date: String!) {
        stop(id: $id) {
        name
        stoptimesForServiceDate(date: $date) {
            stoptimes {
            stopPosition
            scheduledArrival
            realtimeArrival
            arrivalDelay
            scheduledDeparture
            realtimeDeparture
            departureDelay
            timepoint
            realtime
            realtimeState
            pickupType
            dropoffType
            serviceDay
            headsign
            trip {
                gtfsId
                alerts {
                id
                alertHeaderTextTranslations {
                    text
                    language
                }
                alertDescriptionTextTranslations {
                    text
                    language
                }
                alertUrl
                }
                route {
                gtfsId
                shortName
                longName
                agency {
                    gtfsId
                    name
                }
                color
                textColor
                }
            }
            }
        }
        }
    }

"""

Q_SEARCH_DETAILS_BY_STOP = """query GetStopDetails($id: String!, $date: String!) {
        stop(id: $id) {
    	    id
          gtfsId
          vehicleMode
          code
          name
          lat
          lon
        stoptimesForServiceDate(date: $date) {
            stoptimes {
            stopPosition
            scheduledArrival
            realtimeArrival
            arrivalDelay
            scheduledDeparture
            realtimeDeparture
            departureDelay
            timepoint
            realtime
            realtimeState
            pickupType
            dropoffType
            serviceDay
            headsign
            trip {
                gtfsId
                alerts {
                id
                alertHeaderTextTranslations {
                    text
                    language
                }
                alertDescriptionTextTranslations {
                    text
                    language
                }
                alertUrl
                }
                route {
                gtfsId
                shortName
                longName
                agency {
                    gtfsId
                    name
                }
                color
                textColor
                }
            }
            }
        }
        }
    }"""

Q_SEARCH_SCHEDULE_BY_STOP = """query GetStopSchedule($id: String!, $date: String!) {
  stop(id: $id) {
        id
        gtfsId
        vehicleMode
        code
        name
        lat
        lon
    stoptimesForServiceDate(date: $date) {
      stoptimes {
        scheduledDeparture
        realtime
        trip {
          gtfsId
          tripHeadsign
          route {
            gtfsId
            shortName
            color
            textColor
          }
        }
      }
    }
  }
}"""


async def _get_stops() -> dict[str, list[dict]]:
    results = await client.query(Q_SEARCH_STOPS)
    return results.data

async def _get_arrivals(id, date) -> dict[str, list[dict]]:
    results = await client.query(Q_SEARCH_ARRIVALS_BY_STOP, variables={'id': id, 'date': date})
    return results.data

async def _get_details(id, date) -> dict[str, list[dict]]:
    results = await client.query(Q_SEARCH_DETAILS_BY_STOP, variables={'id': id, 'date': date})
    return results.data

async def _get_schedule(id, date) -> dict[str, list[dict]]:
    results = await client.query(Q_SEARCH_SCHEDULE_BY_STOP, variables={'id': id, 'date': date})
    return results.data

async def figure_out_stop_enum(stop: dict) -> str:
    # TODO: make this less cursed
    if stop['vehicleMode'] == 'BUS':
        return stop['gtfsId'].split(':')[0]
    elif stop['vehicleMode'] == 'RAIL':
        return 'SŽ'
    else:
        return 'DEFAULT'
    
stop_background_colors = {
    'DEFAULT': '#000000',
    'MARPROM': '#800000',
    'CELEBUS': '#FFD700',
    'SŽ': '#00A8EB',
    'IJPP': '#004E96',
    'LPP': '#016A42',
    'ARRIVAMP': '#24B7C7'
}

stop_icon_colors = {
    'DEFAULT': '#FFFFFF',
    'MARPROM': '#FFFFFF',
    'CELEBUS': '#000000',
    'SŽ': '#FFFFFF',
    'IJPP': '#FFFFFF',
    'LPP': '#FFFFFF',
    'ARRIVAMP': '#FFFFFF'
}

async def _get_alerts(id) -> dict[str, list[dict]]:
    results = await client.query(Q_SEARCH_ALERTS)
    alerts_all = {}
    for alert in results.data['alerts']:
        if alert['entities'][0]['__typename'] == 'StopOnTrip':
            trip_id = alert['entities'][0]['trip']['gtfsId']
            stop_id = alert['entities'][0]['stop']['gtfsId']
            if stop_id == id:
                if trip_id not in alerts_all:
                    alerts_all[trip_id] = []
                for header_translation, description_translation, url_translation in zip(alert['alertHeaderTextTranslations'], alert['alertDescriptionTextTranslations'], alert['alertUrlTranslations']):
                    alerts_all[trip_id].append(Alert(
                        id=alert['id'],
                        language=header_translation['language'],
                        header=header_translation['text'],
                        description=description_translation['text'],
                        effect=alert['alertEffect'],
                        severity_level=alert['alertSeverityLevel'],
                        cause=alert['alertCause'],
                        trip_id=trip_id,
                        stop_id=stop_id,
                        url=url_translation['text'],
                        start_time=alert['effectiveStartDate'],
                        end_time=alert['effectiveEndDate']
                    ))
    return alerts_all

async def get_stops() -> list[Stop]:
    data = await _get_stops()
    stops = []
    for stop in data['stops']:
        enum = await figure_out_stop_enum(stop)
        if not stop['vehicleMode']:
            continue
        stops.append(Stop(
            id=stop['id'],
            gtfs_id=stop['gtfsId'],
            type=stop['vehicleMode'],
            code=stop['code'],
            name=stop['name'],
            lat=stop['lat'],
            lon=stop['lon'],
            background_color=stop_background_colors.get(enum, '#000000'),
            icon_color=stop_icon_colors.get(enum, '#FFFFFF')
        ))
    return stops

# k: trip_id, v: next stop sequence
TRIP_UPDATES = {}

async def load_trip_updates():
    global TRIP_UPDATES
    trip_updates = {}
    async with aiohttp.ClientSession() as session:
        async with session.get(RT_TU_URL) as response:
            data = await response.json()
            for trip_update in data:
                trip_id = trip_update['tripUpdate']['trip']['tripId']
                trip_updates[trip_id] = trip_update['tripUpdate']['stopTimeUpdate'] if trip_update['tripUpdate'].get('stopTimeUpdate') else []
    TRIP_UPDATES = trip_updates

async def get_next_stop_sequence(trip_id: str) -> dict:
    if trip_id in TRIP_UPDATES:
        return TRIP_UPDATES[trip_id]
    return None

async def is_passed_stop(stoptime: dict, stop_id: str) -> bool:
    next_stop_sequence = await get_next_stop_sequence(stoptime['trip']['gtfsId'])
    if next_stop_sequence and stoptime['stopPosition'] > next_stop_sequence[-1]['stopSequence']:
        # Not passed
        return False
    if next_stop_sequence:
        for stop in next_stop_sequence:
            # If the first stop has a lower stop sequence than the current stop, the vehicle has not passed the stop yet, so we return False
            if stop['stopSequence'] < stoptime['stopPosition']:
                return False
            dept_arrival_time = stop.get('departure') or stop.get('arrival')
            if dept_arrival_time != None and dept_arrival_time.get('time') != None:
                dept_arrival_time = dept_arrival_time['time']
            else:
                dept_arrival_time = None
            if dept_arrival_time == None and stop['stopId'] == stop_id:
                now = datetime.now(pytz.timezone('Europe/Ljubljana'))
                current_time = (now - now.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds()
                if current_time < stoptime['realtimeDeparture']:
                    return False
                continue
            if stop['stopId'] == stop_id and int(dept_arrival_time) < int(datetime.now().timestamp()):
                return True
            if stop['stopId'] == stop_id:
                return False
    elif next_stop_sequence == None:
        # check based on realtime departure
        now = datetime.now(pytz.timezone('Europe/Ljubljana'))
        current_time = (now - now.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds()
        if current_time < stoptime['realtimeDeparture']:
            return False
    return True

async def get_arrivals(id, date, current) -> list[Arrival]:
    data = await _get_arrivals(id, date)
    arrivals = []
    now = datetime.now(pytz.timezone('Europe/Ljubljana'))
    current_time = (now - now.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds()

    alerts_all = await _get_alerts(id)

    for stoptime_group in data['stop']['stoptimesForServiceDate']:
        for stoptime in stoptime_group['stoptimes']:
            if current and current_time > stoptime['realtimeDeparture']:
                continue
            passed = await is_passed_stop(stoptime, id)
            if current and passed:
                continue
            alerts = []
            #for alert in stoptime['trip']['alerts']:
            #    for header_translation, description_translation in zip(alert['alertHeaderTextTranslations'], alert['alertDescriptionTextTranslations']):
            #        alerts.append(Alert(
            #            id=alert['id'],
            #            header=header_translation['text'],
            #            description=description_translation['text'],
            #            language=header_translation['language'],
            #            url=alert['alertUrl']
            #        ))

            # Search for alerts
            if alerts_all.get(stoptime['trip']['gtfsId']):
                alerts = alerts_all[stoptime['trip']['gtfsId']]

            # SŽ warning of uncertainty of delay
            if stoptime['trip']['route']['agency']['gtfsId'] == 'IJPP:1161' and stoptime['arrivalDelay'] == 0 and stoptime['realtime']:
                alerts.append(Alert(
                    id='SZ-UNCERTAIN',
                    header='Opozorilo o morebitni negotovosti časa prihoda',
                    description='Zaradi neprijavljenih zamud vlakov manj kot 5 minut, je možno da pride odstopa časa prihoda oz. odhoda za maksimalno 5 minut.',
                    language='sl',
                    url=None,
                    severity_level='INFO',
                    effect='UNKNOWN',
                    cause='OTHER',
                    trip_id=stoptime['trip']['gtfsId'],
                    stop_id=id,
                    start_time=math.floor(datetime.now().timestamp()),
                    end_time=math.floor(datetime.now().timestamp() + 60*60*24)
                ))
                alerts.append(Alert(
                    id='SZ-UNCERTAIN',
                    header='Warning of possible uncertainty in arrival time',
                    description='Due to unreported train delays of less than 5 minutes, the arrival and departure time of the train may not be accurate.',
                    language='en',
                    url=None,
                    severity_level='INFO',
                    effect='UNKNOWN',
                    cause='OTHER',
                    trip_id=stoptime['trip']['gtfsId'],
                    stop_id=id,
                    start_time=math.floor(datetime.now().timestamp()),
                    end_time=math.floor(datetime.now().timestamp() + 60*60*24)
                ))
            elif stoptime['trip']['route']['agency']['gtfsId'] == 'IJPP:1161' and stoptime['arrivalDelay'] > 0 and stoptime['realtime']:
                alerts.append(Alert(
                    id='SZ-DELAY',
                    header=f'Vlak {stoptime["trip"]["route"]["shortName"]} ima zamudo {math.ceil(stoptime["arrivalDelay"]/60)} min',
                    description=f'Vlak {stoptime["trip"]["route"]["shortName"]} ima prijavljeno zamudo {math.ceil(stoptime["arrivalDelay"]/60)} minut ob prihodu na postajo {data["stop"]["name"]}.',
                    language='sl',
                    url=None,
                    severity_level='WARNING',
                    effect='DELAY',
                    cause='OTHER',
                    trip_id=stoptime['trip']['gtfsId'],
                    stop_id=id,
                    start_time=math.floor(datetime.now().timestamp()),
                    end_time=math.floor(datetime.now().timestamp() + 60*60*24)
                ))
                alerts.append(Alert(
                    id='SZ-DELAY',
                    header=f'Train {stoptime["trip"]["route"]["shortName"]} is delayed by {math.ceil(stoptime["arrivalDelay"]/60)} min',
                    description=f'Train {stoptime["trip"]["route"]["shortName"]} is delayed by {math.ceil(stoptime["arrivalDelay"]/60)} minutes upon arrival at station {data["stop"]["name"]}.',
                    language='en',
                    url=None,
                    severity_level='WARNING',
                    effect='DELAY',
                    cause='OTHER',
                    trip_id=stoptime['trip']['gtfsId'],
                    stop_id=id,
                    start_time=math.floor(datetime.now().timestamp()),
                    end_time=math.floor(datetime.now().timestamp() + 60*60*24)
                ))
            arrivals.append(Arrival(
                agency_id=stoptime['trip']['route']['agency']['gtfsId'],
                agency_name=stoptime['trip']['route']['agency']['name'],
                route_id=stoptime['trip']['route']['gtfsId'],
                route_short_name=stoptime['trip']['route']['shortName'],
                route_color_background=stoptime['trip']['route']['color'],
                route_color_text=stoptime['trip']['route']['textColor'],
                trip_id=stoptime['trip']['gtfsId'],
                trip_headsign=stoptime['headsign'],
                realtime=stoptime['realtime'],
                realtime_status=stoptime['realtimeState'],
                arrival_scheduled=stoptime['scheduledArrival'],
                arrival_realtime=stoptime['realtimeArrival'],
                arrival_delay=stoptime['arrivalDelay'],
                departure_scheduled=stoptime['scheduledDeparture'],
                departure_realtime=stoptime['realtimeDeparture'],
                departure_delay=stoptime['departureDelay'],
                alerts=alerts,
                passed=passed
            ))

    # Sort by arrival time
    arrivals.sort(key=lambda x: x.departure_realtime or x.departure_scheduled or x.arrival_realtime or x.arrival_scheduled)
    return arrivals

async def get_schedule(id, date) -> list[Arrival]:
    data = await _get_schedule(id, date)
    schedule = {}
    for stoptime_group in data['stop']['stoptimesForServiceDate']:
        for stoptime in stoptime_group['stoptimes']:
            # check if we have this route/headsign in the schedule
            st_id = stoptime['trip']['route']['gtfsId'] + stoptime['trip']['tripHeadsign']
            if st_id not in schedule:
                schedule[st_id] = ScheduleRouteEntry(
                    route_id=stoptime['trip']['route']['gtfsId'],
                    route_short_name=stoptime['trip']['route']['shortName'],
                    route_color_background='#' + (stoptime['trip']['route'].get('color') or '000000'),
                    route_color_text='#' + (stoptime['trip']['route'].get('textColor') or 'FFFFFF'),
                    trip_headsign=stoptime['trip']['tripHeadsign'],
                    times=[]
                )
            schedule[st_id].times.append(ScheduleEntry(
                trip_id=stoptime['trip']['gtfsId'],
                departure_time=stoptime['scheduledDeparture'],
                realtime=stoptime['realtime']
            ))
    schedule = [schedule[key] for key in schedule]
    # Sort by departure time
    for route in schedule:
        route.times.sort(key=lambda x: x.departure_time)
    # sort by route short name while converting it to int
    schedule.sort(key=lambda x: int(re.sub(r'\D', '', x.route_short_name), 10) if re.sub(r'\D', '', x.route_short_name) else 999)

    schedule_wrapper = Schedule(
        id=data['stop']['id'],
        gtfs_id=data['stop']['gtfsId'],
        type=data['stop']['vehicleMode'],
        code=data['stop']['code'],
        name=data['stop']['name'],
        lat=data['stop']['lat'],
        lon=data['stop']['lon'],
        schedule=schedule
    )

    return schedule_wrapper
    

async def get_details(id, date, current) -> StopDetails:
    data = await _get_details(id, date)
    arrivals = []
    now = datetime.now(pytz.timezone('Europe/Ljubljana'))
    current_time = (now - now.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds()

    alerts_all = await _get_alerts(id)

    for stoptime_group in data['stop']['stoptimesForServiceDate']:
        for stoptime in stoptime_group['stoptimes']:
            if current and current_time > stoptime['realtimeDeparture']:
                continue
            passed = await is_passed_stop(stoptime, id)
            if current and passed:
                continue
            alerts = []
            # Search for alerts
            if alerts_all.get(stoptime['trip']['gtfsId']):
                alerts = alerts_all[stoptime['trip']['gtfsId']]

            # SŽ warning of uncertainty of delay
            if stoptime['trip']['route']['agency']['gtfsId'] == 'IJPP:1161' and stoptime['arrivalDelay'] == 0 and stoptime['realtime']:
                alerts.append(Alert(
                    id='SZ-UNCERTAIN',
                    header='Opozorilo o morebitni negotovosti časa prihoda',
                    description='Zaradi neprijavljenih zamud vlakov manj kot 5 minut, je možno da pride odstopa časa prihoda oz. odhoda za maksimalno 5 minut.',
                    language='sl',
                    url=None,
                    severity_level='INFO',
                    effect='UNKNOWN',
                    cause='OTHER',
                    trip_id=stoptime['trip']['gtfsId'],
                    stop_id=id,
                    start_time=math.floor(datetime.now().timestamp()),
                    end_time=math.floor(datetime.now().timestamp() + 60*60*24)
                ))
                alerts.append(Alert(
                    id='SZ-UNCERTAIN',
                    header='Warning of possible uncertainty in arrival time',
                    description='Due to unreported train delays of less than 5 minutes, the arrival and departure time of the train may not be accurate.',
                    language='en',
                    url=None,
                    severity_level='INFO',
                    effect='UNKNOWN',
                    cause='OTHER',
                    trip_id=stoptime['trip']['gtfsId'],
                    stop_id=id,
                    start_time=math.floor(datetime.now().timestamp()),
                    end_time=math.floor(datetime.now().timestamp() + 60*60*24)
                ))
            elif stoptime['trip']['route']['agency']['gtfsId'] == 'IJPP:1161' and stoptime['arrivalDelay'] > 0 and stoptime['realtime']:
                alerts.append(Alert(
                    id='SZ-DELAY',
                    header=f'Vlak {stoptime["trip"]["route"]["shortName"]} ima zamudo {math.ceil(stoptime["arrivalDelay"]/60)} min',
                    description=f'Vlak {stoptime["trip"]["route"]["shortName"]} ima prijavljeno zamudo {math.ceil(stoptime["arrivalDelay"]/60)} minut ob prihodu na postajo {data["stop"]["name"]}.',
                    language='sl',
                    url=None,
                    severity_level='WARNING',
                    effect='DELAY',
                    cause='OTHER',
                    trip_id=stoptime['trip']['gtfsId'],
                    stop_id=id,
                    start_time=math.floor(datetime.now().timestamp()),
                    end_time=math.floor(datetime.now().timestamp() + 60*60*24)
                ))
                alerts.append(Alert(
                    id='SZ-DELAY',
                    header=f'Train {stoptime["trip"]["route"]["shortName"]} is delayed by {math.ceil(stoptime["arrivalDelay"]/60)} min',
                    description=f'Train {stoptime["trip"]["route"]["shortName"]} is delayed by {math.ceil(stoptime["arrivalDelay"]/60)} minutes upon arrival at station {data["stop"]["name"]}.',
                    language='en',
                    url=None,
                    severity_level='WARNING',
                    effect='DELAY',
                    cause='OTHER',
                    trip_id=stoptime['trip']['gtfsId'],
                    stop_id=id,
                    start_time=math.floor(datetime.now().timestamp()),
                    end_time=math.floor(datetime.now().timestamp() + 60*60*24)
                ))
            arrivals.append(Arrival(
                agency_id=stoptime['trip']['route']['agency']['gtfsId'],
                agency_name=stoptime['trip']['route']['agency']['name'],
                route_id=stoptime['trip']['route']['gtfsId'],
                route_short_name=stoptime['trip']['route']['shortName'],
                route_color_background=f'#{stoptime["trip"]["route"]["color"]}' if stoptime['trip']['route']['color'] is not None else '#000000',
                route_color_text=f'#{stoptime["trip"]["route"]["textColor"]}' if stoptime['trip']['route']['textColor'] is not None else '#FFFFFF',
                trip_id=stoptime['trip']['gtfsId'],
                trip_headsign=stoptime['headsign'],
                realtime=stoptime['realtime'],
                realtime_status=stoptime['realtimeState'],
                arrival_scheduled=stoptime['scheduledArrival'],
                arrival_realtime=stoptime['realtimeArrival'],
                arrival_delay=stoptime['arrivalDelay'],
                departure_scheduled=stoptime['scheduledDeparture'],
                departure_realtime=stoptime['realtimeDeparture'],
                departure_delay=stoptime['departureDelay'],
                alerts=alerts,
                passed=passed
            ))

    arrivals.sort(key=lambda x: x.departure_realtime or x.departure_scheduled or x.arrival_realtime or x.arrival_scheduled)
    enum = await figure_out_stop_enum(data['stop'])
    return StopDetails(
        id=data['stop']['id'],
        gtfs_id=data['stop']['gtfsId'],
        type=data['stop']['vehicleMode'],
        code=data['stop']['code'],
        name=data['stop']['name'],
        lat=data['stop']['lat'],
        lon=data['stop']['lon'],
        arrivals=arrivals,
        alerts=[],
        background_color=stop_background_colors.get(enum, '#000000'),
        icon_color=stop_icon_colors.get(enum, '#FFFFFF')
    )