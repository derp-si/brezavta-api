from generated.graphql_client import Client

from brezavta_api import config

client = Client(url=config.OTP_URL + '/otp/gtfs/v1')
