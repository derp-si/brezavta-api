from brezavta_api.models.parking import Parking
from .opentripplanner import client
from ..util import many_some_none


async def get_parking():
    data = await client.get_parkings()
    
    parkings = []
    
    for parking in data.vehicleParkings:
        if 'OSM' in parking.vehicleParkingId:
            continue
        # Should be reconsidered, however for the time being, we are only interested in car parkings with dynamic data
        if not parking.realtime:
            continue
        parkings.append(Parking(
            id=parking.vehicleParkingId,
            name=parking.name,
            lat=parking.lat,
            lon=parking.lon,
            capacity=parking.capacity.carSpaces,
            available=max(parking.availability.carSpaces, 0) if parking.realtime else None,
            realtime=parking.realtime,
            icon='NO_DATA' if not parking.realtime else many_some_none(parking.capacity.carSpaces, parking.availability.carSpaces),
        ))
        
    return parkings