import logging
from pathlib import Path

import aiohttp

from brezavta_api import config
from brezavta_api.models.micromobility import Micromobility, MicromobilityDetail, Vehicle, MicromobilityType, MicromobilityForm
from brezavta_api.util import many_some_none

from generated.graphql_client import Client

client = Client(url=config.OTP_URL + '/otp/gtfs/v1')

Q_SEARCH_MICROMOBILITY = (Path(__file__).parent / 'micromobility.gql').read_text()


async def _get_micromobility():
    data = await client.get_micromobility_stations()
    stations = {s.stationId: s for s in data.vehicleRentalStations}
    floating = {s.vehicleId: s for s in data.rentalVehicles}
    return stations, floating


async def get_micromobility():
    stations, vehicles = await _get_micromobility()

    micromobilities = []

    for station in stations.values():
        micromobilities.append(Micromobility(
            id=station.stationId,
            type=MicromobilityType.STATION,
            form=form_for_system(station.rentalNetwork.networkId),
            name=station.name,
            lat=station.lat,
            lon=station.lon,
            vehicles=station.availableVehicles.total,
            spaces=station.availableSpaces.total,
            network=station.rentalNetwork.networkId,
            active=station.operative,
            icon=many_some_none(station.availableSpaces.total, station.availableVehicles.total),
        ))

    for vehicle in vehicles.values():
        micromobilities.append(Micromobility(
            id=vehicle.vehicleId,
            type=MicromobilityType.FLOATING,
            form=vehicle.vehicleType.formFactor.split('_')[0],  # Split to handle SCOOTER_STANDING
            name=vehicle.name,
            lat=vehicle.lat,
            lon=vehicle.lon,
            vehicles=1,
            spaces=0,
            # if we can parse the network as int, add "greengo_" in front of the network name
            network='greengo_' + vehicle.rentalNetwork.networkId if vehicle.rentalNetwork.networkId.isdigit() else vehicle.rentalNetwork.networkId,
            active=True,
            icon='SINGLE',
        ))

    return micromobilities


def form_for_system(system):
    if system.startswith('avant2go') or system == 'shrengo':
        return MicromobilityForm.CAR
    if system == 'kvik' or system == 'bolt':
        return MicromobilityForm.SCOOTER
    return MicromobilityForm.BICYCLE


async def get_micromobility_detail(micromobility_id: str):
    stations, _ = await _get_micromobility()
    station = stations[micromobility_id]
    system, station_id = micromobility_id.split(':')

    vehicles = []
    # TODO: fallback vehicle list...if we care...

    detail = MicromobilityDetail(
        id=station.stationId,
        name=station.name,
        lat=station.lat,
        lon=station.lon,
        network=system,
        vehicles=station.availableVehicles.total,
        spaces=station.availableSpaces.total,
        vehicle_list=vehicles,
        active=station.operative,
        icon=many_some_none(station.availableSpaces.total, station.availableVehicles.total),
        type=MicromobilityType.STATION,
        form=form_for_system(station.rentalNetwork.networkId)
    )
    try:
        detail = await augment_detail(detail)
    except Exception:
        logging.exception(f'Failed to augment station for system {system}')

    return detail


async def augment_detail(detail: MicromobilityDetail):
    system, station_id = detail.id.split(':')
    path = system.split('_')[0]
    if system.startswith('jcdecaux'):
        city = system.split('_')[-1]
        path = f'jcdecaux/{city}'

    async with aiohttp.ClientSession() as session:
        async with session.get(f'https://api.modra.ninja/{path}/vehicles', params={'station_id': station_id}) as response:
            if response.status != 200:
                return detail
            data = await response.json()

    vehicles = []
    for entry in data:
        vehicles.append(Vehicle(
            id=entry['id'],
            type=form_for_system(system),
            model=entry.get('model'),
            image_url=entry.get('image_url'),
            deeplink=entry.get('deeplink'),
            plate=entry.get('license_plate'),
            battery=entry.get('charge_level'),
            range=entry.get('range_estimate'),
            price_start=entry.get('price_start'),
            price_minimum=entry.get('price_minimum'),
            price_per_min=entry.get('price_per_min'),
            price_per_km=entry.get('price_per_km'),
        ))
    detail.vehicle_list = vehicles
    return detail
