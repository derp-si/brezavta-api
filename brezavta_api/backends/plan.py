from brezavta_api.models.plan import PlanWrapper, Plan, Leg, Point, Trip, LegType, LegPoint, Step, Direction, StopWithTime
from brezavta_api.models.alerts import Alert
from pydantic_geojson import LineStringModel

import polyline

from typing import List

from generated.graphql_client import FindPlanPlanItinerariesLegsFrom, FindPlanPlanItinerariesLegsTo
from .opentripplanner import client

async def _get_plan(from_point, to_point, date, time, transport_modes, page_cursor, arrive_by, wheelchair, num_itinearies):
    otp_transport_modes = convert_transport_modes(transport_modes)
    variables = {
        'from_lat': float(from_point['lat']),
        'from_lon': float(from_point['lon']),
        'to_lat': float(to_point['lat']),
        'to_lon': float(to_point['lon']),
        'date': date,
        'time': time,
        'transportModes': otp_transport_modes,
        'pageCursor': page_cursor,
        'arriveBy': arrive_by,
        'wheelchair': wheelchair,
        'num_itineraries': num_itinearies
    }
    base_modes = [mode for mode in otp_transport_modes if mode.get('qualifier', '') != 'RENT']
    rent_modes = [mode for mode in otp_transport_modes if mode.get('qualifier', '') == 'RENT']

    # If there are at least two of BICYCLE_RENT, SCOOTER_RENT or CAR_RENT modes, we need to do multiple queries
    if len(rent_modes) > 1:
        # Split into one object (base, non-rent modes) and one list of objects (rent modes)
        # Get base results
        data = await _get_plan(from_point, to_point, date, time, ','.join([mode['mode'] for mode in base_modes]), page_cursor, arrive_by, wheelchair, num_itinearies)
        # Get rent results
        for rent_mode in rent_modes:
            rent_data = await _get_plan(from_point, to_point, date, time, f"{rent_mode['mode']}_{rent_mode['qualifier']}", page_cursor, arrive_by, wheelchair, num_itinearies)
            # Append rent results to base results
            data.plan.itineraries.extend(rent_data.plan.itineraries)

        # Sort by start time
        data.plan.itineraries = sorted(data.plan.itineraries, key=lambda x: x.startTime)
        return data

    results = await client.find_plan(**variables)
    return results

async def get_plan(from_point, to_point, date, time, transport_modes, page_cursor, arrive_by, wheelchair, num_itinearies) -> PlanWrapper:
    itineraries = await _get_plan(from_point, to_point, date, time, transport_modes, page_cursor, arrive_by, wheelchair, num_itinearies)
    plans = []
    
    for itinerary in itineraries.plan.itineraries:
        legs = []
        for leg in itinerary.legs:
            from_point = convert_point(leg.from_)
            to_point = convert_point(leg.to)

            trip = None
            if leg.trip:
                trip = Trip(
                    id=leg.trip.id,
                    short_name=leg.route.shortName,
                    long_name=leg.route.longName,
                    headsign=leg.trip.tripHeadsign,
                    route_id=leg.route.gtfsId,
                    gtfs_id=leg.trip.gtfsId,
                    color=f'#{leg.route.color}' if leg.route.color is not None else '#000000',
                    text_color=f'#{leg.route.textColor}' if leg.route.textColor is not None else '#FFFFFF'
                )
            geometry = None
            if leg.legGeometry:
                geometry = polyline.decode(leg.legGeometry.points)
            alerts = []
            for alert in leg.alerts:
                entities = alert.entities
                if entities:
                    if entities[0].typename__ == 'StopOnTrip':
                        trip_id = entities[0].trip.gtfsId
                        stop_id = entities[0].stop.gtfsId
                        if stop_id == from_point.name:
                            for header_translation, description_translation, url_translation in zip(alert['alertHeaderTextTranslations'], alert['alertDescriptionTextTranslations'], alert['alertUrlTranslations']):
                                alerts.append(Alert(
                                    id=alert.id,
                                    language=header_translation.language,
                                    header=header_translation.text,
                                    description=description_translation.text,
                                    effect=alert.alertEffect,
                                    severity_level=alert.alertSeverityLevel,
                                    cause=alert.alertCause,
                                    trip_id=trip_id,
                                    stop_id=stop_id,
                                    url=url_translation.text,
                                    start_time=alert.effectiveStartDate,
                                    end_time=alert.effectiveEndDate
                                ))
            stops = []
            for stop in leg.intermediatePlaces or []:
                stop_inner = stop.stop
                stops.append(StopWithTime(
                    id=stop_inner.id,
                    gtfs_id=stop_inner.gtfsId,
                    type=stop_inner.vehicleMode,
                    code=stop_inner.code,
                    name=stop_inner.name,
                    lat=stop_inner.lat,
                    lon=stop_inner.lon,
                    arrival_time=stop.arrivalTime,
                    departure_time=stop.departureTime
                ))
            steps = []
            for step in leg.steps or []:
                steps.append(Step(
                    distance=step.distance,
                    lat=step.lat,
                    lon=step.lon,
                    relative_direction=getattr(Direction, step.relativeDirection, Direction.UNKNOWN),
                    street_name=step.streetName,
                    stay_on=step.stayOn
                ))
            legs.append(Leg(
                mode=getattr(LegType, leg.mode, LegType.UNKNOWN),
                is_rental=getattr(leg, 'rentedBike') or False,  # This is null sometimes (like for BUS and RAIL)
                start_time=leg.startTime,
                end_time=leg.endTime,
                from_point=from_point,
                to_point=to_point,
                geometry=LineStringModel(coordinates=[(coordinate[1], coordinate[0]) for coordinate in geometry]) if geometry else None,
                intermediate_stops=stops,
                trip=trip,
                alerts=alerts,
                realtime=leg.realTime or False,
                realtime_status=leg.realtimeState,
                steps=steps
            ))
        plans.append(Plan(
            start_time=itinerary.startTime,
            end_time=itinerary.endTime,
            legs=legs
        ))
    return PlanWrapper(
        plans=plans,
        previous_page=itineraries.plan.previousPageCursor,
        next_page=itineraries.plan.nextPageCursor
    )
  
def convert_transport_modes(transport_modes: List[str]):
    """Convert a list of string modes (e.g. TRANSIT,BICYCLE_RENT,CAR_RENT) into GraphQL TransportMode objects (e.g. [{mode:TRANSIT},{mode:BICYCLE,qualifier:RENT}...])"""
    modes = []
    for mode in transport_modes.split(','):
        parts = mode.split('_')
        otp_mode = {
            'mode': parts[0]
        }
        if parts[-1] == 'RENT':
            otp_mode['qualifier'] = 'RENT'
        modes.append(otp_mode)
    return modes


def convert_point(p: FindPlanPlanItinerariesLegsFrom | FindPlanPlanItinerariesLegsTo):
    return Point(
        lat=p.lat,
        lon=p.lon,
        departure_time=p.departureTime,
        arrival_time=p.arrivalTime,
        name=p.name,
        type=getattr(LegPoint, p.name.upper(), LegPoint.INTERMEDIATE),
        vertex_type=p.vertexType,
        gtfs_id=p.stop.gtfsId if p.stop else None,
        micromobility_id=p.vehicleRentalStation.stationId if p.vehicleRentalStation else None
    )