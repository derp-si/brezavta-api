from aiographql.client import GraphQLClient
import aiohttp

from brezavta_api import config
from brezavta_api.models.trips import Stop, StopTime, Trip, Alert, LineStringTrip, LineStringProperties
from brezavta_api.models.vehicles import Vehicle, VehicleLocation

from datetime import datetime, timedelta
import pytz
import polyline
import math


client = GraphQLClient(
    endpoint=config.OTP_URL + '/otp/gtfs/v1',
)

RT_VP_URL = 'https://rt.gtfs.ijpp.prometko.si/combined/rt/vl'
RT_TU_URL = 'https://rt.gtfs.ijpp.prometko.si/combined/rt/tu?type=json'

Q_SEARCH_TRIP = """
query GetTrip($id: String!, $date: String!) {
  trip (id: $id) {
    id
    gtfsId
    tripShortName
    tripHeadsign
    routeShortName
  	route {
      id
      gtfsId
      mode
      agency {
        gtfsId
        name
      }
      color
      textColor
    }
    alerts {
      id
      alertHeaderTextTranslations {
				language
        text
			}
    }
    stoptimesForDate (serviceDate: $date) {
      stop {
        id
        gtfsId
        code
        vehicleMode
        name
        lat
        lon
      }
      stopPosition
      scheduledArrival
      realtimeArrival
      arrivalDelay
      scheduledDeparture
      realtimeDeparture
      departureDelay
      realtime
      realtimeState
    }
  }
}
"""

Q_SEARCH_TRIP_GEOMETRY = """
query GetTripGeometry($id: String!) {
  trip (id: $id) {
    route {
      color
    }
    tripGeometry {
      points
    }
  }
}
"""

Q_SEARCH_VEHICLE_LOCATION = """
query GetVehicleLocations {
  patterns {
    id
    route {
      gtfsId
      shortName
      color
    }
    code
    vehiclePositions {
      vehicleId
      label
      lat
      lon
      heading
      speed
      stopRelationship {
        status
        stop {
          id
          gtfsId
          name
          vehicleMode
          code
          lat
          lon
        }
      }
      lastUpdated
      trip {
        gtfsId
        tripHeadsign
        route {
          agency {
            gtfsId,
            name
          }
        }
      }
    }
  }
}
"""

Q_SEARCH_ALERTS = """
query Alerts {
  alerts {
    id
    feed
    alertHeaderTextTranslations {
      text
      language
    }
    alertDescriptionTextTranslations {
      text
      language
    }
    alertEffect
    alertUrlTranslations {
      text
      language
    }
    alertSeverityLevel
    alertCause
    effectiveStartDate
    effectiveEndDate
    entities {
      __typename
      ... on StopOnTrip {
        trip {
          gtfsId
        }
        stop {
          gtfsId
        }
      }
    }
  }
}
"""

Q_VEHICLES_METADATA = """
query GetVehiclesMetadata {
  routes {
    gtfsId
    color
    agency {
      gtfsId
      name
    }
    shortName
  }
  trips {
      gtfsId
      tripHeadsign
  }
  stops {
    id
    gtfsId
    name
    vehicleMode
    code
    lat
    lon
  }
}
"""

VEHICLE_LOCATIONS = []

STOPS = {}
ROUTES = {}
TRIPS = {}

async def get_vehicle_metadata():
    results = await client.query(Q_VEHICLES_METADATA)
    return results.data

async def get_vehicle_positions_metadata():
    global STOPS, ROUTES, TRIPS
    metadata = await get_vehicle_metadata()
    stops = {}
    routes = {}
    trips = {}
    for stop in metadata['stops']:
        enum = await figure_out_stop_enum(stop)or None
        if not stop['vehicleMode']:
            continue
        stops[stop['gtfsId']] = Stop(
            id=stop['id'],
            name=stop['name'],
            code=stop['code'],
            gtfs_id=stop['gtfsId'],
            type=stop['vehicleMode'],
            background_color=stop_background_colors.get(enum, '#000000'),
            icon_color=stop_icon_colors.get(enum, '#FFFFFF'),
            lat=stop['lat'],
            lon=stop['lon'],
        )
    for route in metadata['routes']:
        routes[route['gtfsId']] = route
        routes[route['gtfsId']]['color'] = f'#{route["color"] or "000000"}'
    for trip in metadata['trips']:
        trips[trip['gtfsId']] = trip['tripHeadsign']
      
    STOPS = stops
    ROUTES = routes
    TRIPS = trips
    print('Cached metadata. Stops:', len(stops), 'Routes:', len(routes), 'Trips:', len(trips))


async def _get_vehicle_positions():
    async with aiohttp.ClientSession() as session:
        async with session.get(RT_VP_URL) as response:
            return await response.json()
        
async def get_vehicle_positions():
    global VEHICLE_LOCATIONS, STOPS, ROUTES, TRIPS
    data = await _get_vehicle_positions()

    if not STOPS:
      print('No stops cached, we will wait for metadata')
      return []
      
    vehicles = []
    for vehicle in data:
        if vehicle['vehicle'].get('stopId') not in STOPS or vehicle['vehicle'].get('position') is None:
            continue
        try:
          vehicles.append(
              VehicleLocation(
                  vehicle=Vehicle(
                      id=vehicle['vehicle']['vehicle']['id'],
                      gtfs_id=vehicle['vehicle']['vehicle']['id'],
                      model=None,
                      low_floor=None,
                      image_url=None,
                      plate=vehicle['vehicle']['vehicle'].get('licensePlate') or vehicle['vehicle']['vehicle'].get('label') or vehicle['vehicle']['vehicle']['id'],
                      operator_id=ROUTES.get(vehicle['vehicle']['trip']['routeId'], {}).get('agency', {}).get('gtfsId') or "",
                      operator_name=ROUTES.get(vehicle['vehicle']['trip']['routeId'], {}).get('agency', {}).get('name') or "",
                  ),
                  stop=STOPS.get(vehicle['vehicle']['stopId']),
                  stop_status=vehicle['vehicle']['currentStatus'],
                  lat=vehicle['vehicle']['position']['latitude'],
                  lon=vehicle['vehicle']['position']['longitude'],
                  heading=vehicle['vehicle']['position'].get('bearing') or 0,
                  speed=vehicle['vehicle']['position'].get('speed') or 0,
                  timestamp=vehicle['vehicle']['timestamp'],
                  trip_id=vehicle['vehicle']['trip']['tripId'],
                  color=ROUTES.get(vehicle['vehicle']['trip']['routeId'], {}).get('color') or '#000000',
                  trip_headsign=TRIPS.get(vehicle['vehicle']['trip']['tripId'], ''),
                  route_short_name=ROUTES.get(vehicle['vehicle']['trip']['routeId'], {}).get('shortName') or "",
          ))
        except Exception as e:
            print('Error while parsing vehicle:', e)
    VEHICLE_LOCATIONS = vehicles
    print('Cached vehicle location, count:', len(vehicles))
    return vehicles

TRIP_UPDATES = {}

async def load_trip_updates_for_trips():
    global TRIP_UPDATES
    trip_updates = {}
    async with aiohttp.ClientSession() as session:
        async with session.get(RT_TU_URL) as response:
            data = await response.json()
            for trip_update in data:
                trip_id = trip_update['tripUpdate']['trip']['tripId']
                trip_updates[trip_id] = trip_update['tripUpdate']['stopTimeUpdate'] if trip_update['tripUpdate'].get('stopTimeUpdate') else []
    TRIP_UPDATES = trip_updates

async def get_next_stop_sequence(trip_id: str) -> dict:
    if trip_id in TRIP_UPDATES:
        return TRIP_UPDATES[trip_id]
    return None

async def is_passed_stop(stoptime: dict, trip_id: str, stop_id: str, stoptime_previous: dict) -> bool:
    if stoptime_previous and stoptime_previous.passed == False:
        return False
    next_stop_sequence = await get_next_stop_sequence(trip_id)
    if next_stop_sequence:
        for stop in next_stop_sequence:
            dept_arrival_time = stop.get('departure') or stop.get('arrival')
            if dept_arrival_time != None and dept_arrival_time.get('time') != None:
                dept_arrival_time = dept_arrival_time['time']
            else:
                dept_arrival_time = None
            if dept_arrival_time == None:
                now = datetime.now(pytz.timezone('Europe/Ljubljana'))
                current_time = (now - now.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds()
                if current_time < stoptime['realtimeDeparture']:
                    return False
                continue
            if stop['stopId'] == stop_id and int(dept_arrival_time) < int(datetime.now().timestamp()):
                return True
            if stop['stopId'] == stop_id:
                return False
    elif next_stop_sequence == None:
        # check based on realtime departure
        now = datetime.now(pytz.timezone('Europe/Ljubljana'))
        current_time = (now - now.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds()
        if current_time < stoptime['realtimeDeparture']:
            return False
    return True

async def _get_trip(id, date) -> dict[str, list[dict]]:
    if date is None:
        date = datetime.now(pytz.timezone('Europe/Ljubljana')).strftime('%Y%m%d')
    results = await client.query(Q_SEARCH_TRIP, variables={'id': id, 'date': date})
    return results.data

async def _get_trip_geometry(id) -> dict[str, list[dict]]:
    results = await client.query(Q_SEARCH_TRIP_GEOMETRY, variables={'id': id})
    return results.data

async def figure_out_stop_enum(stop: dict) -> str:
    # TODO: make this less cursed
    if stop['vehicleMode'] == 'BUS':
        return stop['gtfsId'].split(':')[0]
    elif stop['vehicleMode'] == 'TRAIN':
        return 'SŽ'
    
stop_background_colors = {
    'DEFAULT': '#000000',
    'MARPROM': '#800000',
    'CELEBUS': '#FFD700',
    'SŽ': '#00A8EB',
    'IJPP': '#004E96',
    'LPP': '#016A42',
    'ARRIVAMP': '#24B7C7'
}

stop_icon_colors = {
    'DEFAULT': '#FFFFFF',
    'MARPROM': '#FFFFFF',
    'CELEBUS': '#000000',
    'SŽ': '#FFFFFF',
    'IJPP': '#FFFFFF',
    'LPP': '#FFFFFF',
    'ARRIVAMP': '#FFFFFF'
}

async def _get_alerts(id) -> dict[str, list[dict]]:
    results = await client.query(Q_SEARCH_ALERTS)
    alerts_all = {}
    for alert in results.data['alerts']:
        if alert['entities'][0]['__typename'] == 'StopOnTrip':
            trip_id = alert['entities'][0]['trip']['gtfsId']
            stop_id = alert['entities'][0]['stop']['gtfsId']
            if trip_id == id:
                if stop_id not in alerts_all:
                    alerts_all[stop_id] = []
                for header_translation, description_translation, url_translation in zip(alert['alertHeaderTextTranslations'], alert['alertDescriptionTextTranslations'], alert['alertUrlTranslations']):
                    alerts_all[stop_id].append(Alert(
                        id=alert['id'],
                        language=header_translation['language'],
                        header=header_translation['text'],
                        description=description_translation['text'],
                        effect=alert['alertEffect'],
                        severity_level=alert['alertSeverityLevel'],
                        cause=alert['alertCause'],
                        trip_id=trip_id,
                        stop_id=stop_id,
                        url=url_translation['text'],
                        start_time=alert['effectiveStartDate'],
                        end_time=alert['effectiveEndDate']
                    ))
    return alerts_all

async def _get_vehicles() -> dict[str, list[dict]]:
    url = 'https://ojpp.si/api/vehicles/'
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            return await response.json()

async def _get_vehicle_models() -> dict[str, list[dict]]:
    url = 'https://ojpp.si/api/vehicle-models/'
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            return await response.json()

async def _get_vehicle_locations() -> dict[str, list[dict]]:
    results = await client.query(Q_SEARCH_VEHICLE_LOCATION)
    return results.data


async def get_trip(id, date) -> list[Stop]:
    data = await _get_trip(id, date)
    alerts_all = await _get_alerts(id)
    stoptimes = []
    if not data['trip']:
        return []
    for stoptime in data['trip']['stoptimesForDate']:
        stop = stoptime['stop']
        enum = await figure_out_stop_enum(stop)
        stop_local = Stop(
            id=stop['id'],
            name=stop['name'],
            code=stop['code'],
            gtfs_id=stop['gtfsId'],
            type=stop['vehicleMode'],
            lat=stop['lat'],
            lon=stop['lon'],
            background_color=str(stop_background_colors.get(enum, '#000000')),
            icon_color=str(stop_icon_colors.get(enum, '#FFFFFF'))
        )
        alerts = []
        if alerts_all.get(stop['gtfsId']):
            alerts = alerts_all[stop['gtfsId']]
        # SŽ warning of uncertainty of delay
        if data['trip']['route']['agency']['gtfsId'] == 'IJPP:1161' and stoptime['arrivalDelay'] == 0 and stoptime['realtime']:
            alerts.append(Alert(
                id='SZ-UNCERTAIN',
                header='Opozorilo o morebitni negotovosti časa prihoda',
                description='Zaradi neprijavljenih zamud vlakov manj kot 5 minut, je možno da pride odstopa časa prihoda oz. odhoda za maksimalno 5 minut.',
                language='sl',
                url=None,
                severity_level='INFO',
                effect='UNKNOWN',
                cause='OTHER',
                trip_id=id,
                stop_id=str(stop['gtfsId']),
                start_time=math.floor(datetime.now().timestamp()),
                end_time=math.floor(datetime.now().timestamp() + 60*60*24)
            ))
            alerts.append(Alert(
                id='SZ-UNCERTAIN',
                header='Warning of possible uncertainty in arrival time',
                description='Due to unreported train delays of less than 5 minutes, the arrival and departure time of the train may not be accurate.',
                language='en',
                url=None,
                severity_level='INFO',
                effect='UNKNOWN',
                cause='OTHER',
                trip_id=id,
                stop_id=str(stop['gtfsId']),
                start_time=math.floor(datetime.now().timestamp()),
                end_time=math.floor(datetime.now().timestamp() + 60*60*24)
            ))
        elif data['trip']['route']['agency']['gtfsId'] == 'IJPP:1161' and stoptime['arrivalDelay'] > 0 and stoptime['realtime']:
            alerts.append(Alert(
                id='SZ-DELAY',
                header=f'Vlak {data["trip"]["routeShortName"]} ima zamudo {math.ceil(stoptime["arrivalDelay"]/60)} min',
                description=f'Vlak {data["trip"]["routeShortName"]} ima prijavljeno zamudo {math.ceil(stoptime["arrivalDelay"]/60)} minut ob prihodu na postajo {stop["name"]}.',
                language='sl',
                url=None,
                severity_level='WARNING',
                effect='DELAY',
                cause='OTHER',
                trip_id=id,
                stop_id=str(stop['gtfsId']),
                start_time=math.floor(datetime.now().timestamp()),
                end_time=math.floor(datetime.now().timestamp() + 60*60*24)
            ))
            alerts.append(Alert(
                id='SZ-DELAY',
                header=f'Train {data["trip"]["routeShortName"]} is delayed by {math.ceil(stoptime["arrivalDelay"]/60)} min',
                description=f'Train {data["trip"]["routeShortName"]} is delayed by {math.ceil(stoptime["arrivalDelay"]/60)} minutes upon arrival at station {stop["name"]}.',
                language='en',
                url=None,
                severity_level='WARNING',
                effect='DELAY',
                cause='OTHER',
                trip_id=id,
                stop_id=str(stop['gtfsId']),
                start_time=math.floor(datetime.now().timestamp()),
                end_time=math.floor(datetime.now().timestamp() + 60*60*24)
            ))
        stoptime = StopTime(
            stop=stop_local,
            sequence=stoptime['stopPosition'],
            arrival_scheduled=stoptime['scheduledArrival'],
            arrival_realtime=stoptime['realtimeArrival'],
            arrival_delay=stoptime['arrivalDelay'],
            departure_scheduled=stoptime['scheduledDeparture'],
            departure_realtime=stoptime['realtimeDeparture'],
            departure_delay=stoptime['departureDelay'],
            realtime=stoptime['realtime'],
            realtime_status=stoptime['realtimeState'],
            alerts=alerts,
            passed=await is_passed_stop(stoptime, id, str(stop['gtfsId']), stoptimes[-1] if stoptimes else None)
        )
        stoptimes.append(stoptime)
    trip = Trip(
        id=data['trip']['id'],
        gtfs_id=data['trip']['gtfsId'],
        route_id=data['trip']['route']['gtfsId'],
        route_short_name=data['trip']['routeShortName'],
        trip_type=data['trip']['route']['mode'],
        trip_headsign=data['trip']['tripHeadsign'],
        stop_times=stoptimes,
        color=f'#{data["trip"]["route"]["color"]}' if data['trip']['route']['color'] is not None else '#000000',
        text_color=f'#{data["trip"]["route"]["textColor"]}' if data['trip']['route']['textColor'] is not None else '#FFFFFF',
    )
    return trip

async def get_trip_geometry(id) -> list[dict]:
    data = await _get_trip_geometry(id)
    if data['trip']['tripGeometry'] is None:
        return LineStringTrip({
            "type": "LineString",
            "coordinates": [],
            "properties": LineStringProperties(
                color="#000000"
            )
        })
    decoded = polyline.decode(data['trip']['tripGeometry']['points'])
    data = {
        "type": "LineString",
        "coordinates": [[lon, lat] for lat, lon in decoded],
        "properties": LineStringProperties(
            color=f'#{data["trip"]["route"]["color"]}' if data['trip']['route']['color'] is not None else '#000000'
        )
    }
    return LineStringTrip(type=data['type'], coordinates=data['coordinates'], properties=data['properties'])

async def get_vehicle_locations(id) -> list[VehicleLocation]:
    ids = id.split(',')
    vehicles = []
    for vehicle in VEHICLE_LOCATIONS:
        if vehicle.trip_id in ids:
            vehicles.append(vehicle)

    return vehicles

async def get_all_vehicle_locations() -> list[VehicleLocation]:
    return VEHICLE_LOCATIONS
    
async def get_trip_geometry_from_cache(trip_id) -> list[dict]:
    # Fetch from self
    async with aiohttp.ClientSession() as session:
        url = 'http://127.0.0.1:8000/trips/' + trip_id + '/geometry'
        async with session.get(url) as response:
            data = await response.json()
            return LineStringTrip(**data)