import aiohttp
from brezavta_api.models.geocoding import SearchResult
import editdistance

NOMATIM_URL = 'http://nominatim.brdo.pirnet.si'

async def nominatim_autocomplete(input, components='country:si', **kwargs):
    url = f'{NOMATIM_URL}/search?q={input}&format=geocodejson&addressdetails=1'
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            return await response.json()
        
def get_description(feature):
    description = ''
    if 'address' in feature['properties']:
        address = feature['properties']['address']
        if 'road' in address:
            description += address['road']
        if 'house_number' in address:
            description += ' ' + address['house_number']
        if 'city' in address:
            description += ', ' + address['city']
    else:
        geocoding = feature['properties']['geocoding']
        if 'street' in geocoding:
            description += geocoding['street']
        if 'housenumber' in geocoding:
            description += ' ' + geocoding['housenumber']
        if 'locality' in geocoding:
            description += geocoding['locality'] if len(description) == 0 else ', ' + geocoding['locality']
        if 'postcode' in geocoding:
            description += geocoding['postcode'] if len(description) == 0 else ', ' + geocoding['postcode']
        if 'city' in geocoding:
            description += geocoding['city'] if len(description) == 0 else ', ' + geocoding['city']
        if 'county' in geocoding:
            description += geocoding['county'] if len(description) == 0 else ', ' + geocoding['county']
        if 'state' in geocoding:
            description += geocoding['state'] if len(description) == 0 else ', ' + geocoding['state']
    return description

def convert_places_autocomplete(data, string):
    results = []
    for feature in data['features']:
        if feature['properties']['geocoding'].get('name') is None:
            feature['properties']['geocoding']['name'] = f'{feature["properties"]["geocoding"]["street"]} {feature["properties"]["geocoding"]["housenumber"]}'
        mx = max(len(feature['properties']['geocoding']['name']), len(string))
        results.append(SearchResult(
            label=feature['properties']['geocoding']['name'],
            confidence=mx / (mx + editdistance.eval(feature['properties']['geocoding']['name'], string)),
            description=get_description(feature),
            type='geocode',
            lat=feature['geometry']['coordinates'][1],
            lon=feature['geometry']['coordinates'][0],
        ))
    return results


async def reverse_geocode(lat, lon):
    url = f'{NOMATIM_URL}/reverse?lat={lat}&lon={lon}&format=geocodejson&addressdetails=1'
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            return await response.json()
        
def calculate_coordinate_confidence(lat, lon, feature):
    lat_diff = abs(lat - feature['geometry']['coordinates'][1])
    lon_diff = abs(lon - feature['geometry']['coordinates'][0])
    return 1 / (1 + lat_diff + lon_diff)

def convert_reverse_geocode(data, lat, lon):
    results = []
    for feature in data['features']:
        results.append(SearchResult(
            label=feature['properties']['geocoding']['name'],
            confidence=calculate_coordinate_confidence(lat, lon, feature),
            description=get_description(feature),
            type='geocode',
            lat=lat,
            lon=lon,
        ))
    return results