import os

IS_DEV = os.getenv("ENVIRONMENT", default="dev").startswith("dev")

SENTRY_DSN = os.getenv("SENTRY_DSN", default=None)

USE_CACHE = not IS_DEV

USE_SENTRY = SENTRY_DSN and not IS_DEV

OTP_URL = 'https://otp.ojpp-gateway.derp.si'