from fastapi.testclient import TestClient

from brezavta_api.main import app
from brezavta_api.models.micromobility import MicromobilityDetail

client = TestClient(app)

def test_micromobility_list():
    response = client.get("/micromobility/")
    assert response.status_code == 200

def test_micromobility_avant():
    response = client.get("/micromobility/avant2go_si:56be71605f40d3ba38e6f484")
    assert response.status_code == 200
    data = MicromobilityDetail.model_validate(response.json())
    vehicle_count = sum(len(s.vehicle_list) for s in data.vehicle_list)
    # assert data.image_url is not None, "Station does not have an image"
