FROM python:3.12-slim

WORKDIR /app
EXPOSE 80

RUN pip install pipenv

COPY Pipfile* .
RUN pipenv install --system --deploy

COPY . .

RUN mkdir -p generated/graphql_client/ && ariadne-codegen

CMD ["uvicorn", "brezavta_api.main:app", "--host", "", "--port", "80"]
